<?php
$errorMsg="";
if ($_SERVER['REQUEST_METHOD'] !== 'POST'){
    redirectNow();
}
$sfpEmail = '';
$sfpName = '';
$sfpBody = '';
$has_Error = false;
if ($_POST) {
    if ($_POST['sfp-secret']) {
        $has_Error = true;
        redirectNow();
    }
    if ($_POST['sfp-name'] && preg_match('/^[a-zA-Z0-9\s]+$/', $_POST['sfp-name'])) {
        $sfpName = filter_var($_POST['sfp-name'], FILTER_SANITIZE_STRING);
    }
    if ($_POST['sfp-email'] && filter_var($_POST['sfp-email'], FILTER_VALIDATE_EMAIL)) {
        $sfpEmail = filter_var($_POST['sfp-email'], FILTER_SANITIZE_EMAIL);
    } else {
        $has_Error = true;
        redirectNow();
    }
    if ($_POST['sfp-body']) {
        $sfpBody = filter_var($_POST['sfp-body'], FILTER_SANITIZE_STRING);
    } else {
        $has_Error = true;
        redirectNow();
    }
    if (isset($_POST['sfp-agreement']) && !filter_var($_POST['sfp-agreement'], FILTER_VALIDATE_BOOLEAN)) {
        $has_Error = true;
        redirectNow();
    }
} else {
    $has_Error=true;
}
if (!$has_Error) {
    $to = 'david@sitedesignusa.com';
    $subject = 'An email from static form page';
    $message = 'Name: ' . $sfpName . "\r\n" . $sfpBody;
    $headers = 'From: root@fake044.racknerdfake044.net' . "\r\n" .
        'Reply-To: ' . $sfpEmail . "\r\n" .
        'X-Mailer: PHP/' . phpversion();
    mail($to, $subject, $message, $headers);

    redirectNow();
} else {
    redirectNow();
}

function redirectNow () {
    header("Location: ../../thankYou.php");
}
