
// get form
let form = document.querySelector("form#sfp-form")

form.querySelectorAll("button").forEach(el=>{
    el.addEventListener("click", (e)=>{
        e.preventDefault()
        if(validateData()){
            form.submit()
        } else {
            document.querySelectorAll("p.formError").forEach(erel => {
                erel.style.display = "block"
            })
        }
    })
})

function validateData(){
    if (!form.querySelector("input#sfp-email").value) { return false}
    if (!form.querySelector("textarea#sfp-body").value) { return false}
    if (form.querySelector("input#sfp-agreement").checked === false) { return false}
    if (form.querySelector("input[type=hidden][name=sfp-secret]").value) { return false}

    return true
}