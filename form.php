<!DOCTYPE html>
<html lang="en">
<head>
    <title>Your Form</title>
    <link href="/styles/style.css" rel="stylesheet">
</head>
<body>
<h1>Here is your form!</h1>
<div>
    <p class="formError" style="display: none;"><span style="background-color: #ff6666">Error: Not all fields are filled out.</span></p>
    <form id="sfp-form" action="scripts/php/script.php" method="post">
        <div>
        <input type="hidden" name="sfp-secret" readonly>
        <label for="sfp-name">Full Name:</label>
        </div>
        <div>
        <input type="text" name="sfp-name" id="sfp-name">
        </div>
        <div>
        <label for="sfp-email">E-Mail Address: *</label>
        </div>
        <div>
        <input type="email" name="sfp-email" id="sfp-email" required>
        </div>
        <div>
        <label for="sfp-body">Enquiry, Article Submission, Questions or Comments: *</label>
        </div>
        <div>
        <textarea id="sfp-body" name="sfp-body" required></textarea>
        </div>
        <div>
            <label for="sfp-agreement">Do you agree? *</label>
            <input type="checkbox" id="sfp-agreement" name="sfp-agreement">
        </div>
        <p>* required.</p>
        <div>
        <button type="button">Submit</button>
        </div>

    </form>
</div>
<script src="scripts/js/script.js"></script>
</body>
</html>